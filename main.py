# Standard Library Imports
from flask import Flask, redirect, url_for, request, render_template
from datetime import datetime
import csv

app = Flask(__name__)



@app.route('/dashboard')
def dashboard():
    # send this to dashboard as input.
    #bus_occupancy_data = fill_bus_occupancy_graph_data()
    route_density_info = None
    return render_template('dashboard.html')


# @app.route('/stackedbarchart')
# def staked_bar_chart():
#     return render_template('stackedbarchart.html')
#
# @app.route('/horizontalbarchart')
# def horizontal_bar_chart():
#     return render_template('horizontalbarchart.html')


@app.route('/')
def linechart():
    return render_template('linechart.html')

@app.route('/userinput')
def user_input():
    import json
    json_data = request.args.get('data')
    print (json_data)
    json_data = json.loads(json_data)
    source = json_data['source']
    destination = json_data['destination']
    route_map = {
                 "Route1": ["Bellandur", "Kadubesanahalli"],
                 "Route2": ["Hebbal", "Kundanahalli"]
                }

    cur_route = "Route1"
    # for key, valie_list in route_map:
    #     if source in valie_list:
    #         cur_route = key
    # cur_route = "Route1"

    sample_data = json.loads(open('./static/sample.json').read())
    for info_list in sample_data['info']:
        print (info_list, cur_route)
        if info_list['bus_route'] == cur_route:
            info_list["average_density"] = int(info_list["average_density"]) + 11
            with open('./static/sample.json', 'w') as outfile:
                json.dump(sample_data, outfile)

    cur_time = datetime.now().strftime("%H%M")
    # Mitigation
    mitigation_data = {
        "bus_route": "Route1",
        "rule": "send_extra_buses",
        "action": "In Progress"
    }
    with open('./static/.json', 'w') as file:
        json.dump(mitigation_data,file)

    return_val = '{"data":{"response": "OK"}}';
    return return_val

@app.route('/userinputtest')
def user_input_test():
    import json
    json_data = request.args.get('data')
    print (json_data)
    json_data = json.loads(json_data)
    source = json_data['source']
    destination = json_data['destination']
    route_map = {
                 "Route1": ["Bellandur", "Kadubesanahalli"],
                 "Route2": ["Hebbal", "Kundanahalli"]
                }

    # cur_route = "Route1"
    for key, valie_list in route_map.items():
        if source in valie_list:
            cur_route = key

    sample_data = json.loads(open('./static/sample.json').read())
    for info_list in sample_data['info']:
        print (info_list, cur_route)
        if info_list['bus_route'] == cur_route:
            info_list["average_density"] = int(info_list["average_density"]) + 11
            with open('./static/sample.json', 'w') as outfile:
                json.dump(sample_data, outfile)

    cur_time = datetime.now().strftime("%H%M")
    # Mitigation
    mitigation_data = {
        "bus_route": cur_route,
        "rule": "send_extra_buses",
        "action": "In Progress"
    }
    with open('./static/mitigate.json', 'w') as file:
        json.dump(mitigation_data,file)

    return_val = '{"response": "OK"}';
    return return_val

if __name__ == '__main__':
    app.run(host='172.22.88.254', debug=True)

    # Step 1 : Prepare the historical data before reading the API
    #          Implementation Notes : Why? Wait till it is needed.

