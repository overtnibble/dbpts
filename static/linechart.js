// Our labels along the x-axis
var hour = ["06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00"];
// For drawing the lines
var monday = [10,25,30,50,35,30,30,24,22,20,25,50,45,30,25,20,10];
var tuesday = [10,20,32,48,31,28,28,21,19,18,26,55,47,32,23,13,10];
var wednesday = [09,22,32,52,37,28,27,21,20,18,22,43,42,28,23,21,08];
var thursday = [11,23,33,60,39,34,35,22,21,18,22,80,80,78,40,23,17];
var friday = [14,22,44,42,31,31,28,22,21,18,22,47,40,28,22,18,08];
var saturday = [04,09,16,20,25,27,29,22,15,14,20,35,38,32,45,55,35];
var sunday = [02,05,09,14,68,70,65,22,19,18,25,24,20,14,12,09,05];

var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: hour,
    datasets: [
      { 
        data: monday,
        label: "Monday",
        borderColor: "#3e95cd",
        fill: false
      },
      { 
        data: tuesday,
        label: "Tuesday",
        borderColor: "#8e5ea2",
        fill: false
      },
      { 
        data: wednesday,
        label: "Wednesday",
        borderColor: "#3cba9f",
        fill: false
      },
      { 
        data: thursday,
        label: "Thursday",
        borderColor: "#e8c3b9",
        fill: false
      },
      { 
        data: friday,
        label: "Friday",
        borderColor: "#f45850",
        fill: false
      },
      {
        data: saturday,
        label: "Saturday",
        borderColor: "#44aa50",
        fill: false
      },
      {
        data: sunday,
        label: "Sunday",
        borderColor: "#8aaea2",
        fill: false
      }
    ]
  }
});