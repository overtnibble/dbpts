 // Return with commas in between
  var numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

var dataPack1 = [];
var dataPack2 = [];
var routes = [];
var routesInfo = [];

$.ajaxSetup({
    async: false
});
$.getJSON('../static/sample.json', function(response) {
    routesInfo = response.info;
});

$.each(routesInfo,function(key,value){

 routes.push(value.bus_route);
 var threshold = 50;
 if(value.average_density <= 50)
 {
    dataPack1.push( parseInt(value.average_density));
    dataPack2.push(0);
 }
 else
 {
 dataPack1.push(threshold);
    dataPack2.push(parseInt(value.average_density)-threshold);
 }
});


var bar_ctx = document.getElementById('myStackedBarChart');

var bar_chart = new Chart(bar_ctx, {
    type: 'bar',
    data: {
        labels: routes,
        datasets: [
        {
            label: 'WithinThreshold',
            data: dataPack1,
						backgroundColor: "rgba(0, 100, 0, 0.7)",
						hoverBackgroundColor: "rgba(0, 100, 0, 0.7)",
						hoverBorderWidth: 2,
						hoverBorderColor: 'lightgrey'
        },
        {
            label: 'ExceedsThreshold',
            data: dataPack2,
						backgroundColor: "rgba(225, 58, 55, 0.7)",
						hoverBackgroundColor: "rgba(225, 58, 55, 0.7)",
						hoverBorderWidth: 2,
						hoverBorderColor: 'lightgrey'
        },
        ]
    },
    options: {
     		animation: {
        	duration: 10,
        },
        tooltips: {
					mode: 'label',
          callbacks: {
          label: function(tooltipItem, data) {
          	return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
          }
          }
         },
        scales: {
          xAxes: [{
          	stacked: true,
            gridLines: { display: false },
            }],
          yAxes: [{
          	stacked: true,
            ticks: {
        			callback: function(value) { return numberWithCommas(value); },
     				},
            }],
        }, // scales
        legend: {display: true}
    } // options
   }
);